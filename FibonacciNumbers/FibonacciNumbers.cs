﻿using System;
using System.Windows.Forms;
using System.Threading;//для Thread.Sleep()

namespace FibonacciNumbers
{
    public partial class FibonacciNumbers : Form
    {
        public FibonacciNumbers()
        {
            InitializeComponent();
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            int count=0;
            try
            {
                count = Convert.ToInt32(tbCount.Text);
            }
            catch
            {
                MessageBox.Show("Неверное выражение в поле количества чисел");
            }

            Computing(count);
        }

        public void Computing(int count)
        {
            progressBar.Value = 0;

            Int64 fib1 = 1, fib2=1;

            rtbOutput.Text = "";

            if(count==1)
            {
                rtbOutput.Text = fib1.ToString();
                progressBar.Value = progressBar.Maximum;
            }

            else if(count == 2)
            {
                rtbOutput.Text = fib1.ToString();
                progressBar.Value = progressBar.Maximum/2;
                Thread.Sleep(200);

                rtbOutput.Text += "  "+fib2.ToString();
                progressBar.Value = progressBar.Maximum;
            }

            else if(count>2)
            {
                Int64 resFib;

                rtbOutput.Text = fib1.ToString();
                progressBar.Value = (int)(progressBar.Maximum / count);
                Thread.Sleep(200);

                rtbOutput.Text += "  " + fib2.ToString();
                progressBar.Value = (int)(progressBar.Maximum * 2/ count);

                for (int i=3; i<= count; i++)
                {
                    resFib = fib1;
                    fib1 += fib2;
                    fib2 = resFib;
                    Thread.Sleep(200);
                    progressBar.Value = (int)(progressBar.Maximum * i / count);

                    rtbOutput.Text += "  " + fib1.ToString();
                    rtbOutput.Update();
                }

            }
        }
    }
}
